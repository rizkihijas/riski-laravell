<?php

namespace App\Http\Controllers;

use App\Models\rizky;
use Illuminate\Http\Request;

class RizkyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tampil = rizky::all();
        return view('tampil', compact('tampil'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('buat');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        rizky::create([
            'nama'=>$request->nama,
            'alamat'=>$request->alamat,
            'tetala'=>$request->tetala,
        ]);
        return redirect('tampil');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\rizky  $rizky
     * @return \Illuminate\Http\Response
     */
    public function show(rizky $id)
    {
        $tampilsatu = rizky::find($id);
        return view('tampilsatu', compact('tampilsatu'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\rizky  $rizky
     * @return \Illuminate\Http\Response
     */
    public function edit(rizky $rizky, $id)
    {
       $ubah = rizky::findorfail($id);
       return view('ubah',compact('ubah')); 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\rizky  $rizky
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $ubah = rizky::findorfail($id);
        $ubah->update($request->all());

        return redirect('tampil');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\rizky  $rizky
     * @return \Illuminate\Http\Response
     */
    public function destroy(rizky $rizky, $id)
    {
        $ubah = rizky::findorfail($id);
        $ubah->delete();

        return back();
    }
}
