<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\RizkyController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('content');
});

Route::get('tampil', [RizkyController::class, 'index']);
Route::get('tampil/{id}', [RizkyController::class, 'show']);
Route::get('buat', [RizkyController::class, 'create']);
Route::post('simpan', [RizkyController::class, 'store']);
Route::get('ubah/{id}', [RizkyController::class, 'edit']);
Route::post('update/{id}', [RizkyController::class, 'update']);
Route::get('hapus/{id}', [RizkyController::class, 'destroy']);