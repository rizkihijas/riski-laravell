<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\rizky;

class RizkySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Rizky::create([
            'nama' => 'Moh Rizky', 
            'alamat' => 'Panglemah',
            'tetala' => 'pamekasan,28 08 2001',
        ]);
    }
}
